package produit;

public class EBilletLeconIndividuelle extends EBillet {

	/**
	 * Contructeur qui initialise un e-billet pour une lecon individuelle
	 * 
	 * @param l
	 * 
	 * 		Libelle du produit
	 * 
	 * @param prix
	 * 
	 * 		Prix du produit
	 */
	public EBilletLeconIndividuelle(String l, double prix) {
		
		super(l,prix);
		this.type = "E-Billet";
		set("typeProd",type);
}
}