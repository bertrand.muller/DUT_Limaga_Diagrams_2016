package produit;

import activeRecord.Modele;

public class Produit extends Modele {
	
	/**
	 * Attribut representant le nom du produit
	 */
	private String libelle;
	
	/**
	 * Attribut representant le prix unitaire du produit
	 * 
	 */
	private double prixUnitaire;
	
	/**
	 * Attribut representant le type du produit
	 */
	protected String type = "";
	
	/**
	 * Constructeur qui initialise un produit
	 * 
	 * @param l
	 * 
	 * 		Nom du produit
	 * 
	 * @param prix
	 * 
	 * 		Prix unitaire du produit
	 */
	
	public Produit(String l, double prix) {
		
		super();
		set("libelle", l);
		set("prixUnitaire", prix);	
		set("typeProd", type);
		
	}

}
