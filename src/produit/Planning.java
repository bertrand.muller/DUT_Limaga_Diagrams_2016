package produit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Planning {
	
	
	/**
	 * Map associant un produit a une liste de
	 * dates pour lesquelles le produit est deja reserve
	 */
	public static Map<Produit, ArrayList<String>> mapProduitDates;
	
	public Planning() {
		
		mapProduitDates = new HashMap<Produit,ArrayList<String>>();
		
	}
	
	/**
	 * Methode qui permet de reserver une journee
	 * 
	 * @param p
	 * 
	 * 		Produit reserve
	 * 
	 * @param date
	 * 
	 * 		Date a laquelle le produit est reserve
	 */
	
	public void reserverJournee(Produit p, String date) {
		
		ArrayList<String> listDates = this.getDatesProduit(p);
		
		if(this.verifierDate(date)) {
			listDates.add(date);
			mapProduitDates.put(p,listDates);
		}
	}
	
	/**
	 * Methode qui permet de verifier la disponibilite d'un produit
	 * 
	 * @param date
	 * 
	 * 		Date qui doit etre verifiee
	 * 
	 * @return
	 * 
	 * 		Retourne "true" si le produit est libre, "false" sinon
	 */
	public boolean verifierDate(String date) {
		
		if(mapProduitDates.containsKey(date)) {
			return false;
		} else {
			return true;
		}
		
	}
	
	/**
	 * Methode qui permet l'ensemble des dates reservees pour un produit
	 * 
	 * @param p
	 * 
	 * 		Produit sur lequel les dates doivent etre retourne
	 * 
	 * @return
	 * 
	 * 		Retourne l'ensemble des dates auxquels le produit a ete reserve
	 */
	
	public ArrayList<String> getDatesProduit(Produit p) {
		
		if(this.mapProduitDates.containsKey(p)) {
			return this.mapProduitDates.get(p);
		} else {
			return new ArrayList<String>();
		}
		
	}
}
