package activeRecord;
import java.lang.reflect.Constructor;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * Classe modelisant le modele
 */
public class Modele {
	
	
	/**
	 * Connexion etablie avec la base de donnees
	 */
	protected Connection cnt;
	
	
	/**
	 * Ensemble des attributs du modele
	 */
	protected Map<String , Object> attributs;
	
	
	/**
	 * Numero de l'identifiant du modele
	 */
	protected int id;
	
	
	/**
	 * Constructeur d'un modele
	 */
	public Modele(){
		
		cnt = Connexion.getConnection();	
		attributs = new  LinkedHashMap<String, Object>();
		id = -1;
		
		
	}
	
	
	/**
	 * Methode permettant la valeur d'un attribut
	 * du modele contenu dans la variable attributs
	 * 
	 * @param nom
	 * 		Nom de l'attribut
	 * 
	 * @param valeur
	 * 		Nouvelle valeur de l'attribut
	 */
	public void set(String nom, Object valeur){
		
		attributs.put(nom, valeur); //Permet egalement la mise a jour
					
	}
	
	
	/**
	 * Methode permettant d'obtenir la valeur
	 * d'un attribut a partir de son nom
	 * 
	 * @param nom
	 * 		Nom de l'attribut
	 * 
	 * @return
	 * 		Valeur correspondant au nom de l'attribut
	 */
	public Object get(String nom){
		
		return attributs.get(nom);
		
	}
	
	
	/**
	 * Methode permettant de retourner
	 * l'indentifiant du modele correspondant
	 * 
	 * @return
	 * 		Id du modele
	 */
	public int getId() {
		
		return id;
		
	}
	
	
	/**
	 * Methode permettant de sauvegarder les donnees
	 * du modele en fonction des valeurs contenues dans attributs
	 * 
	 * @throws SQLException
	 * 		Exception SQL eventuellement levee en cas d'erreur lors de l'ajout
	 */
	public void save() throws SQLException{
		
		String nomClasse = "";
		
		if((getClass().getSuperclass().getSuperclass().getSimpleName().equals("Produit")
			|| (getClass().getSuperclass().getSimpleName().equals("Produit")))) {
			
			nomClasse = "Produit";
			
		} else {
			
			String[] classInfo = getClass().getName().split("\\.");
			nomClasse = classInfo[classInfo.length - 1];	
			
		}
		
		Object[] params = new Object[attributs.size() + 1];	
		int i = 0;
		
		
		//On verifie si le modele existe deja dans la base
		if (id == -1){
			
			String requete = "SELECT MAX(id) FROM " + nomClasse;
			ResultSet res = Connexion.executerQuery(cnt, requete);
			res.next();
			int newID = res.getInt(1) + 1;
			
			requete = "INSERT INTO " + nomClasse + " VALUES (";
			
			for(int j = 0 ; j < attributs.size() + 1; j++){
				
				requete += "?,";
			
			}
			
			requete = requete.substring(0, requete.length() - 1) + ")";
			
			params[0] = newID;
			
			for (String key : attributs.keySet()) {

				params[i + 1] = attributs.get(key);
				i++;

			}
			
			Connexion.executerPreparedUpdate(cnt, requete, params);
			
			id = newID;
			
			
		}else{
			
			for(String key : attributs.keySet()){
				
				String requete  = "UPDATE " + nomClasse  + " SET ";
				requete += (key + " = ? WHERE id = ?");				
				params[0] = attributs.get(key);
				params[1] = this.id;
				Connexion.executerPreparedUpdate(cnt, requete, params);
				i++;
			}
		}
		
	}
	
	
	/**
	 * Methode permettant de retrouver un element contenu
	 * dans la base par son identifiant
	 * 
	 * @param id_obj
	 * 		Id de l'element
	 * 
	 * @param classe
	 * 		Classe representant cet objet
	 * 
	 * @return
	 * 		L'objet correspondant a l'id et a sa classe
	 * 
	 * @throws Exception
	 * 		Exception eventuellement levee
	 */
	public static Object findById(int id_obj, Class classe) throws Exception{
		
		String[] classInfo = classe.getName().split("\\.");
		String nomClasse = classInfo[classInfo.length - 1];			
		Object[] params = {id_obj};
		int i = 2;
		
		
		String requete = "SELECT * FROM " + nomClasse + " WHERE id = ?";
		ResultSet res = Connexion.executerPreparedQuery(Connexion.getConnection(), requete, params);
		
		Class tempObj = Class.forName(classe.getName()); 		
		Constructor[] cts = tempObj.getConstructors();
		Class[] typesParametres = cts[0].getParameterTypes();
		Object[] atts = new Object[typesParametres.length];
		ResultSetMetaData rsmd = res.getMetaData();
		res.next();
			
		
		for(int j = 0; j < rsmd.getColumnCount() - 1; j++ ){
					
			atts[j] = res.getObject(i, typesParametres[j]);
			i++;
			
		}
		
		Modele newObj = (Modele) cts[0].newInstance(atts);		
		newObj.id = id_obj;
		
	
		
		return newObj;
		
	}
	
	
	/**
	 * Methode permettant d'avoir la liste des objets
	 * contenus dans la base et associes a une classe
	 * 
	 * @param classe
	 * 		Classe correspondant a l'ensemble des objets souhaites
	 * 
	 * @return
	 * 		Liste des objets correspondants
	 * 
	 * @throws Exception
	 * 		Exception eventuellement levee
	 */
	public static ArrayList<Object> all(Class classe) throws Exception{
		
		String[] classInfo = classe.getName().split("\\.");
		String nomClasse = classInfo[classInfo.length - 1];
		ArrayList<Object> ar = new ArrayList<Object>();
		int i = 0;
		
		String requete = "SELECT * FROM " + nomClasse; 
		ResultSet res = Connexion.executerQuery(Connexion.getConnection(), requete);
		
		while(res.next()){
			
			Object o1 = findById(res.getInt(1), classe);
			System.out.println(res.getInt(1));
			ar.add(o1);
			i++;
			
		}
		
		return ar;

	}
	
	
	/**
	 * Methode permettant de supprimer le modele courant
	 * de la base de donnees
	 * 
	 * @throws SQLException
	 * 		Exception eventuellement levee
	 */
	public void delete() throws SQLException {
		
		if(id != -1) {
			
			String nomClasse = "";
			
			if((getClass().getSuperclass().getSuperclass().getSimpleName().equals("Produit")
				|| (getClass().getSuperclass().getSimpleName().equals("Produit")))) {
				
				nomClasse = "Produit";
				
			} else {
				
				String[] classInfo = getClass().getName().split("\\.");
				nomClasse = classInfo[classInfo.length - 1];	
				
			}
			
			Object[] params = {id};
				
			String requete = "DELETE FROM " + nomClasse + " WHERE id = ?";
			Connexion.executerPreparedUpdate(Connexion.getConnection(), requete, params);
			
		}
	}
}
