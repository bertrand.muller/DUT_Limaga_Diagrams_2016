package activeRecord;


import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 * Classe modelisant une connexion avec la base de donnees
 * Elle permet d'effectuer les operations de selection et de mises a jour
 */
public class Connexion {
	
	private static Connection connection;
	
	
	/**
	 * Methode statique permetant d'initialiser une connexion
	 * avec la base de donnees
	 * 
	 * @return
	 * 		La connection effectuee avec la base de donnees
	 */
	private static void creerConnexion(){
		
		Connection cnt = null;

		//Maison String url="jdbc:oracle:thin:@mc.team-arma.fr:1521:XE";
		//IUT String url="jdbc:oracle:thin:@charlemagne:1521:infodb";
		 
		String url="jdbc:oracle:thin:@mc.team-arma.fr:1521:XE";
		
		 try {
			 //IUT ledoux11u 	test123
			 //Maison iut 	iut1234
			 
			cnt = DriverManager.getConnection(url,"iut","iut1234");
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		 connection = cnt;
		 
	}
	
	public static Connection getConnection(){
		
		if(connection == null){
			
			creerConnexion();
			
		}
		
		return connection;
		
	}

	
	/**
	 * Methode statique permettant d'executer une requete de selection
	 * 
	 * @param cnt
	 * 		Connexion etablie avec la base de donnees
	 * 
	 * @param requete
	 * 		Requete de selection
	 * 
	 * @return
	 * 		Resultat de la requete
	 */
	public static ResultSet executerQuery(Connection cnt, String requete){
		
		ResultSet res = null;
		
		try {
			
			Statement stmt = cnt.createStatement();
			res = stmt.executeQuery(requete);
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		
		return res;
	}
	
	
	/**
	 * Methode statique permettant d'executer une requete de mise a jour
	 * 
	 * @param cnt
	 * 		Connexion etablie avec la base de donnees
	 * 
	 * @param requete
	 * 		Requete de mise a jour
	 * 
	 * @return
	 * 		Valeur indiquant si la mise a jour a ete realisee ou non
	 */
	public static int executerUpdate(Connection cnt, String requete){
		
		int res = 0;
		
		try {
			
			Statement stmt = cnt.createStatement();
			res = stmt.executeUpdate(requete);
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		
		return res;
	}


	/**
	 * Methode statique permettant d'executer une requete de selection parametree
	 * 
	 * @param cnt
	 * 		Connexion etablie avec la base de donnees
	 * 
	 * @param requete
	 * 		Requete de selection
	 * 
	 * @param params
	 * 		Tableau de parametres associes a la requete
	 * 
	 * @return
	 * 		Resultat de la requete
	 */
	public static ResultSet executerPreparedQuery(Connection cnt, String requete, Object[] params) {
		
		ResultSet  res = null;
		
		try {
			
			PreparedStatement stmt = cnt.prepareStatement(requete);
			
			int i=1;
			
			for(Object o : params){	

				
				if(o instanceof String){				
				
				stmt.setString(i, (String) o);
				
				}

				if(o instanceof Integer){				
					
					stmt.setInt(i, (Integer) o);
					
					}
				
				
				if(o instanceof Double){				
					
					stmt.setDouble(i, (Double) o);
					
					}
				
			i++;
				
			}

			res = stmt.executeQuery();
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		
		return res;
	}
	

	/**
	 * Methode statique permettant d'executer une requete de mise a jour parametree
	 * 
	 * @param cnt
	 * 		Connexion etablie avec la base de donnees
	 * 
	 * @param requete
	 * 		Requete de selection
	 * 
	 * @param params
	 * 		Tableau de parametres associes a la requete
	 * 
	 * @return
	 * 		Resultat de la requete
	 */
	public static int executerPreparedUpdate(Connection cnt, String requete, Object[] params) {
		
		int res = 0;
		
		try {
			
			PreparedStatement stmt = cnt.prepareStatement(requete);
			
			int i=1;
			
			for(Object o : params){	

				if(o instanceof Boolean) {
					
					stmt.setBoolean(i, (Boolean)o);
					
				}
				
				
				if(o instanceof String){				
				
					stmt.setString(i, (String) o);
				
				}

				if(o instanceof Integer){				
					
					stmt.setInt(i, (Integer) o);
				
				}
				
				if(o instanceof Date){				
					
					stmt.setDate(i, (Date) o);
				
				}
				
				if(o instanceof Double){				
					
					stmt.setDouble(i, (Double) o);
				
				}
				
			i++;
				
			}

			res = stmt.executeUpdate();
			
		} catch (SQLException e) {
			
			e.printStackTrace();
			
		}
		
		return res;
		
	}
	
}
