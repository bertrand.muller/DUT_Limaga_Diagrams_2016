package application;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import activeRecord.Connexion;
import activeRecord.Modele;

public class Commande extends Modele {
	
	/**
	 * Attribut representant l'identifiant unique du client
	 * ayant realiser la commande
	 */
	public static int numClient;
	
	/**
	 * Constructeur qui initialise une commande avec l'ensemble des information requises
	 * passees en parametes
	 * 
	 * @param c
	 * 
	 * 		Client ayant realise la commande
	 * 	
	 * @param mpay
	 * 
	 * 		Moyen de paiement du client
	 * 
	 * @param paye
	 * 
	 * 		Boolean indiquant si la commande a ete payee ou non
	 * 
	 * @throws ParseException
	 * 
	 * 		Exception retournee en cas d'erreur lors de la creation de la date
	 * 
	 */
	public Commande(Client c, String mpay, Boolean paye) throws ParseException {
		
		super();		
		numClient = c.getId();
		set("dateCom", new Date(System.currentTimeMillis()));
		set("moyenPaiement", mpay);
		set("estPayee", paye);
		set("numClient", c.getId());

	}
	
	/**
	 * Methode qui permet d'acceder a l'attribut id de la commande
	 * 
	 * @return
	 * 
	 * 		L'id de la commande
	 */
	
	public int getNumCommande() {
		
		return this.id;
		
	}
	
	/**
	 *  Methode qui permet de connaitre si une facture est 
	 *  payee ou non
	 *  
	 * @return
	 * 		
	 * 		Retourne "true" si la commande est payee, "false" sinon
	 */
	
	public boolean getEstPayee() {
		
		return (boolean)this.get("estPaye");
		
	}
	
	/**
	 * Methode qui permet de changer l'etat de paiement d'une commande
	 * 
	 * @param b
	 * 
	 * 		Nouvel etat du paiement de la commande 
	 */
	
	public void setEstPayee(boolean b) {
		
		set("estPaye", b);
		
	}
	
	/**
	 * Methode qui permet de payer une commande
	 */
	
	public void payer() {
		
		boolean estPayee = (boolean)this.get("estPayee");
		
		if(!estPayee) {
			
			set("estPayee", true);
			
		}
	}

/**
 * Methode qui permet d'editer la facture d'une commande
 * 
 * @return
 * 
 * 		Facture liee a la commande
 * 
 * @throws SQLException
 * 
 * 		Exception retournee en cas de soucis avec le requetage SQL
 * 	
 */
	
	public Facture creerFacture() throws SQLException {
		
		String requete = "SELECT prix FROM Panier WHERE numClient = ?";
		Object[] params = {(int)this.get("numClient")};
		ResultSet res = Connexion.executerPreparedQuery(Connexion.getConnection(), requete, params);
		res.next();
		
		int total = res.getInt(1);
		
		Facture f = new Facture((Date)this.get("dateCom"), total, 0.2, id);
		f.save();
		f.editerFacture();
		
		return f;
		
	}
}
