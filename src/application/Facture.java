package application;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import activeRecord.Connexion;
import activeRecord.Modele;


/**
 * Classe modelisant une facture
 */
public class Facture extends Modele {
	
	
	/**
	 * Constructeur d'une facture
	 * 
	 * @param d
	 * 		Date de la facture
	 * 
	 * @param tot
	 * 		Montant total HT de la facture
	 * 
	 * @param tva
	 * 		Taux de TVA associe a la facture
	 * 
	 * @param numC
	 * 		Numero de commande associe a la facture
	 */
	public Facture(Date d, double tot, double tva, int numC) {
		
		super();
		set("dateFact", d);
		set("totalHT", tot);
		set("tauxTVA", tva);
		set("numCommande", numC);

	}
	
	
	/**
	 * Methode permettant d'afficher la facture
	 * avec toutes les informations necessaires
	 */
	public void editerFacture() {
		
		double totalHT = (double) this.get("totalHT");
		double tauxTVA = (double) this.get("tauxTVA");
		double totalTTC = totalHT+(totalHT*tauxTVA);
		tauxTVA *= 100;
		
		try {
			
			String ligne;
			BufferedReader fichier = new BufferedReader(new FileReader("fichiers" + File.separatorChar + "facture.txt"));
			
			
			//On recupere les elements necessaires a l'edition d'une facture
			String requete = "SELECT dateCom, nom, prenom, adresse, numTel "
							+ "FROM Client "
							+ "NATURAL JOIN Panier "
							+ "NATURAL JOIN Commande "
							+ "NATURAL JOIN Contient "
							+ "WHERE numClient = ?";
			
			Object[] params = {Commande.numClient};
			ResultSet res = Connexion.executerPreparedQuery(Connexion.getConnection(), requete, params);
			ResultSetMetaData rsmd = res.getMetaData();
			res.next();
			
			
			//On cree un tableau d'elements qui remplaceront les '-'
			String[] tabReplace = new String[20];
			
			
			//On recupere l'id de la facture
			tabReplace[0] = String.valueOf(this.getId());
			
			int i = 1;
				
			
			//On ajoute l'ensemble des elements retournes par la requete dans le tableau tabReplace
			while(i <= rsmd.getColumnCount()) {
				tabReplace[i] = res.getString(i);
				i++;
			}
			
			
			//On ajoute les montants HT et TTC et la TVA
			tabReplace[i] = String.valueOf(totalHT) + " euros";
			tabReplace[i+1] = String.valueOf(tauxTVA) + "%";
			tabReplace[i+2] = String.valueOf(totalTTC) + " euros";
			
			i = 0;
			
			
			//On remplace l'ensemble des '-' par les elements contenus dans tabReplace
			while((ligne = fichier.readLine()) != null) {
				
				if(ligne.contains("-")) {
					
					ligne = ligne.replace("-", tabReplace[i]);
					i++;
				}
				
				System.out.println(ligne);
				
			}
			
			fichier.close();
			
		} catch (Exception e) {
			System.out.println("Erreur avec le fichier 'facture.txt' ! \n" + e.toString());
		} 
	}
	
}
