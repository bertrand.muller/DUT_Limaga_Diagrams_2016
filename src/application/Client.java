package application;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Map;

import produit.Planning;
import produit.Produit;
import activeRecord.Connexion;
import activeRecord.Modele;


/**
 * Classe modelisant un client
 */
public class Client extends Modele {

	
	/**
	 * Constructeur d'un client
	 * 
	 * @param n
	 * 		Nom du client
	 * 
	 * @param p
	 * 		Prenom du client
	 * 
	 * @param dateN
	 * 		Date de naissance du client
	 * 
	 * @param ad
	 * 		Adresse du client
	 * 
	 * @param numT
	 * 		Numero de telephone du client
	 * 
	 * @param niv
	 * 		Niveau de natation du client
	 */
	public Client(String n, String p, Date dateN, String ad, String numT, String niv) {
		
		set("nom", n);
		set("prenom", p);
		set("dateNaiss", dateN);
		set("adresse", ad);
		set("numTel", numT);
		set("NiveauNatation", niv);
	}

	
	/**
	 * Methode permettant de retourner le moyen
	 * de paiement souhaite par le client
	 * 
	 * @return
	 * 		Chaine de caracteres decrivant le moyen de paiement
	 */
	public String demanderMoyenPaiement() {
		
		return "CB";
		
	}
	
	
	/**
	 * Methode permettant d'emettre l'e-billet 
	 * que le client a reserve
	 */
	public void emettreEBillet() {
		
		try {
			
			String ligne;
			BufferedReader fichier = new BufferedReader(new FileReader("fichiers" + File.separatorChar + "ebillet.txt"));
			
			//On cree un tableau contenant les elements remplacant les '-'
			String[] tabReplace = new String[20];
			
			
			//On recupere le nom et le prenom du client
			tabReplace[1] = (String) this.get("nom");
			tabReplace[2] = (String) this.get("prenom");
			
			
			//On recupere le dernier E-Billet edite
			String requete = "SELECT MAX(id) "
							+ "FROM Produit";
			
			ResultSet res = Connexion.executerQuery(cnt, requete);
			res.next();
			tabReplace[0] = String.valueOf(res.getInt(1));
			
			
			//On recupere les dates associees a un produit
			Map<Produit,ArrayList<String>> listDatesProd = Planning.mapProduitDates; 
			
			
			//On cherche un produit ayant le meme id que celui dernierement ajoute
			for(Produit p : listDatesProd.keySet()) {
				
				
				//On recupere la derniere date associee au produit
				if(String.valueOf(p.getId()).equals(tabReplace[0])) {
					ArrayList<String> listDates = listDatesProd.get(p);
					tabReplace[3] = listDates.get(listDates.size()-1);
				}
				
			}
			
			int i = 0;
			
			
			//On remplace les '-' du fichier par les elements contenus dans tabReplace
			while((ligne = fichier.readLine()) != null) {
				
				if(ligne.contains("-")) {
					
					ligne = ligne.replace("-", tabReplace[i]);
					i++;
				}
				
				System.out.println(ligne);
				
			}
			
			fichier.close();
			
		} catch (Exception e) {
			System.out.println("Erreur avec le fichier 'ebillet.txt' ! \n" + e.toString());
		} 
		
	}
}
