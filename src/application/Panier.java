package application;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import activeRecord.Connexion;
import activeRecord.Modele;
import produit.Produit;


/**
 * Classe modelisant un panier
 */
public class Panier extends Modele {

	/**
	 * Client ayant cree le panier
	 */
	private Client cl;
	
	
	/**
	 * Montant en euros du panier
	 */
	private double prix;
	
	
	/**
	 * Atribut permettant d'associer un produit a une quantite
	 */
	private Map<Produit,Integer> quantiteProduits;
	
	
	/**
	 * Constructeur d'un panier
	 */
	public Panier(Client c) {
		
		this.cl = c;
		this.prix = 0;
		this.quantiteProduits = new HashMap<Produit,Integer>();
		
	}
	
	/**
	 * Methode qui permet d'ajouter un produit passe en parametre
	 * dans le panier
	 * 
	 * @param p
	 * 
	 * 		Produit a ajouter dans le panier
	 * 
	 * @param quant
	 * 
	 * 		Quantite du produit a ajouter
	 */
	
	public void ajouterPanier(Produit p, int quant) {
		
		this.quantiteProduits.put(p, quant);
		this.prix += (double)p.get("prixUnitaire")*quant;
		
	}
	
	
	/**
	 * Methode permettant de valider un panier
	 * 
	 * @return
	 * 
	 * 		Commande liee a la validation du panier
	 * 
	 * @throws ParseException
	 * 
	 * 		Exception retournee en cas d'erreurs lors de la creation de la date
	 */
	public Commande validerPanier() throws ParseException {
		
		set("prix",prix);
		set("numClient",cl.getId());
				
		Commande c = new Commande(cl, "", false);
		
		return c;
		
	}
	
	
	/**
	 * Methode qui petrmet de creer la table Contient
	 */
	public void ajouterTableContient() {
		
		String requete = "INSERT INTO Contient VALUES (?,?)";
		Object[] params = {this.getId(),this.get("numClient")};
		Connexion.executerPreparedUpdate(Connexion.getConnection(), requete, params);
		
	}

}
