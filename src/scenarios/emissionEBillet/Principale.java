package scenarios.emissionEBillet;

import java.sql.Date;

import activeRecord.Connexion;
import application.Client;
import application.Commande;
import application.Facture;
import application.Panier;
import produit.EBilletLeconIndividuelle;
import produit.Planning;
import produit.Produit;

public class Principale {

	public static void main(String[] args) throws Exception {
		
		
		//Creation du planning
		Planning pl = new Planning();
		
		
		//Creation du produit que le client veut reserver
		Produit p = new EBilletLeconIndividuelle("Lecon Individuelle", 10);
		p.save();
		
		
		//Creation du client
		Client c = new Client("Heberian", "Voyance", new Date(0), "1 rue Principale", "3615", "Merveilleux");
		c.save();
		
		
		//Creation du panier avec le produit du client
		Panier pa = new Panier(c);
		pl.reserverJournee(p, "01/02/2016");
		pa.ajouterPanier(p, 1);
		
		
		//Recuperation de la commande correspondante
		Commande com = pa.validerPanier();
		com.save();
		
		
		//Enregistrement du panier
		pa.save();
		pa.ajouterTableContient();
		
		
		//Recuperation de la facture correspondant a la commande et enregistrement
		Facture f = com.creerFacture();
		f.save();
		
		
		//Demande du moyen de paiement au client
		com.set("moyenPaiement", c.demanderMoyenPaiement());
		com.save();
		
		
		//Paiement
		com.payer();
		com.save();
		
		
		//Emission de l'e-billet
		c.emettreEBillet();
		
		//Suppression des donnees pour ne pas surcharger la base
		Connexion.executerUpdate(Connexion.getConnection(), "DELETE FROM Contient");
		pa.delete();
		f.delete();
		com.delete();
		c.delete();
		p.delete();
		
	}

}
